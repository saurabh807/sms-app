package com.example.sms;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;

public class DBHelper extends SQLiteOpenHelper {

   public static final String DATABASE_NAME = "MyDBName.db";
   public static final String CONTACTS_TABLE_NAME = "contacts";
   public static final String CONTACTS_COLUMN_ID = "id";
   public static final String CONTACTS_COLUMN_TIME = "entrytime";
   public static final String CONTACTS_COLUMN_PHONE = "phone";
   private HashMap hp;

   public DBHelper(Context context)
   {
      super(context, DATABASE_NAME , null, 1);
   }

   @Override
   public void onCreate(SQLiteDatabase db) {
      // TODO Auto-generated method stub
      db.execSQL(
      "create table contacts " +
      "(id integer primary key, entrytime text,phone text)"
      );
   }

   @Override
   public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
      // TODO Auto-generated method stub
      db.execSQL("DROP TABLE IF EXISTS contacts");
      onCreate(db);
   }

   public boolean insertContact  (String phone, String time)
   {
      SQLiteDatabase db = this.getWritableDatabase();
      ContentValues contentValues = new ContentValues();
      contentValues.put("entrytime", time);
      contentValues.put("phone", phone);
     // db.beginTransaction();
      System.out.println("Insertion starts");
      db.insert("contacts", null, contentValues);
     // db.endTransaction();
      System.out.println("Insertion ends");
      return true;
   }
   
   public Cursor getData(int id){
      SQLiteDatabase db = this.getReadableDatabase();
      Cursor res =  db.rawQuery( "select * from contacts where id="+id+"", null );
      return res;
   }
   
   public int numberOfRows(){
      SQLiteDatabase db = this.getReadableDatabase();
      int numRows = (int) DatabaseUtils.queryNumEntries(db, CONTACTS_TABLE_NAME);
      return numRows;
   }
   
   public boolean updateContact (Integer id,  String phone, String time)
   {
      SQLiteDatabase db = this.getWritableDatabase();
      ContentValues contentValues = new ContentValues();
      contentValues.put("entrytime", time);
      contentValues.put("phone", phone);
      
      db.update("contacts", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
      return true;
   }

   public Integer deleteContact (Integer id)
   {
      SQLiteDatabase db = this.getWritableDatabase();
      return db.delete("contacts", 
      "id = ? ", 
      new String[] { Integer.toString(id) });
   }
   
   public ArrayList<String> getAllCotacts()
   {
      ArrayList<String> array_list = new ArrayList<String>();
      
      //hp = new HashMap();
      SQLiteDatabase db = this.getReadableDatabase();
      Cursor res =  db.rawQuery( "select * from contacts", null );
      res.moveToFirst();
      
      while(res.isAfterLast() == false){
         array_list.add(res.getString(res.getColumnIndex(CONTACTS_COLUMN_PHONE))+res.getString(res.getColumnIndex(CONTACTS_COLUMN_TIME)));
         res.moveToNext();
      }
      if (!res.isClosed()) 
      {
    	  res.close();
      }
   return array_list;
   }
   
   public ArrayList<String> getContactsToday(String phone)
   {
      ArrayList<String> array_list = new ArrayList<String>();
      DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
      Date date = new Date();
      String dt=dateFormat.format(date);
      System.out.println(phone+"  "+dt);
      //hp = new HashMap();
      SQLiteDatabase db = this.getReadableDatabase();
      String[] tableColumns = new String[] {
    		    "phone"
    		};
      String whereClause = "phone = ? AND entrytime = ?";
      String[] whereArgs = new String[] {
          phone,
          dt
      };
      //String orderBy = "column1";
      Cursor res = db.query("contacts", tableColumns, whereClause, whereArgs,
              null, null, null);

      // since we have a named column we can do
      //int idx = c.getColumnIndex("phone");
      
      //db.query("contacts", columns, selection, selectionArgs, groupBy, having, orderBy)
      //Cursor res =  db.rawQuery( "select * from contacts where entrytime="+dt+" and phone="+phone, null );
      res.moveToFirst();
      
      while(res.isAfterLast() == false){
         array_list.add(res.getString(res.getColumnIndex(CONTACTS_COLUMN_PHONE)));
         res.moveToNext();
      }
      if (!res.isClosed()) 
      {
    	  res.close();
      }
      System.out.println(array_list.size()+"");
   return array_list;
   }
   
}

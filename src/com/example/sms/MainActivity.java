package com.example.sms;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {
	
	 DBHelper mydb;
	   
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	
    	
    	TelephonyManager tMgr = (TelephonyManager)getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
    	final String getSimSerialNumber = tMgr.getSimSerialNumber();
    	mydb = new DBHelper(this);
    	//mydb.onUpgrade(mydb.getWritableDatabase(), 1, 2);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        Date date = new Date();
        final String dt=dateFormat.format(date);
        System.out.println(dateFormat.format(date));
        final Button button11 = (Button) findViewById(R.id.refresh);
        
        final Button button = (Button) findViewById(R.id.sim1);
        
    	
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	System.out.println("Button 1 pressed");
            	mydb.insertContact("7869905798",dt);
            	Intent i = new Intent(getApplicationContext(), MyTestService.class);
            	
                // Add extras to the bundle
        		i.putExtra("myno", "7869905798");
        		i.putExtra("mysimno", getSimSerialNumber);
                // Start the service
                startService(i);
                button11.performClick();
                // Perform action on click
            }
        });
        
       
       
        final Button button2 = (Button) findViewById(R.id.sim2);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	System.out.println("Button 2 pressed");
            	
            	mydb.insertContact("7224827172",dt);
            	Intent i = new Intent(getApplicationContext(), MyTestService.class);
                
                // Add extras to the bundle
        		i.putExtra("myno", "7224827172");
        		i.putExtra("mysimno", getSimSerialNumber);
        		  

        		   
                // Start the service
                startService(i);
                // Perform action on click
                button11.performClick();
            }
        });
        
        final Button button3 = (Button) findViewById(R.id.sim3);
        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	mydb.insertContact("sim3no",dt);
            	Intent i = new Intent(getApplicationContext(), MyTestService.class);
                
                // Add extras to the bundle
        		i.putExtra("myno", "sim3no");
        		
                // Start the service
                startService(i);
                // Perform action on click
                button11.performClick();
            }
        });
        
        final Button button4 = (Button) findViewById(R.id.sim4);
        button4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	mydb.insertContact("sim4no",dt);
            	Intent i = new Intent(getApplicationContext(), MyTestService.class);
                
                // Add extras to the bundle
        		i.putExtra("myno", "sim4no");
               
                // Start the service
                startService(i);
                // Perform action on click
                button11.performClick();
            }
        });
        
        final Button button5 = (Button) findViewById(R.id.sim5);
        button5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	mydb.insertContact("sim5no",dt);
            	Intent i = new Intent(getApplicationContext(), MyTestService.class);
               
                // Add extras to the bundle
        		i.putExtra("myno","sim5no");
               
                // Start the service
                startService(i);
                // Perform action on click
                button11.performClick();
            }
        });
        
        final Button button6 = (Button) findViewById(R.id.sim6);
        button6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	mydb.insertContact("sim6no",dt);
            	Intent i = new Intent(getApplicationContext(), MyTestService.class);
               
                // Add extras to the bundle
        		i.putExtra("myno", "sim6no");
               
                // Start the service
                startService(i);
                // Perform action on click
                button11.performClick();
            }
        });
        
        final Button button7 = (Button) findViewById(R.id.sim7);
        button7.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	mydb.insertContact("sim7no",dt);
            	Intent i = new Intent(getApplicationContext(), MyTestService.class);
              
                // Add extras to the bundle
        		i.putExtra("myno", "sim7no");
               
                // Start the service
                startService(i);
                // Perform action on click
                button11.performClick();
            }
        });
        
        final Button button8 = (Button) findViewById(R.id.sim8);
        button8.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	mydb.insertContact("sim8no",dt);
            	Intent i = new Intent(getApplicationContext(), MyTestService.class);
               
                // Add extras to the bundle
        		i.putExtra("myno", "sim8no");
               
                // Start the service
                startService(i);
                // Perform action on click
                button11.performClick();
            }
        });
        
        final Button button9 = (Button) findViewById(R.id.sim9);
        button9.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	mydb.insertContact("sim9no",dt);
            	Intent i = new Intent(getApplicationContext(), MyTestService.class);
              
        		i.putExtra("myno", "sim9no");
              
                startService(i);
                // Perform action on click
                button11.performClick();
            }
        });
        
        final Button button10 = (Button) findViewById(R.id.sim10);
        button10.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	mydb.insertContact("sim10no",dt);
            	Intent i = new Intent(getApplicationContext(), MyTestService.class);
                
                // Add extras to the bundle
        		i.putExtra("myno", "sim10no");
                
                // Start the service
                startService(i);
                // Perform action on click
                button11.performClick();
            }
        });
        
        final Button button12 = (Button) findViewById(R.id.clear);
        button12.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	mydb.onUpgrade(mydb.getWritableDatabase(), 1, 2);
                button11.performClick();
            }
        });
        
        button11.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	
            	ArrayList array_list = mydb.getContactsToday("7869905798"); 
            	System.out.println("test"+array_list.size());
            	if(array_list.size()>0)
                button.setEnabled(false);
            	else
            	button.setEnabled(true);	
            	
            	array_list = mydb.getContactsToday("7224827172"); 
            	System.out.println("test"+array_list.size());
            	if(array_list.size()>0)
                button2.setEnabled(false);
            	else
               	button2.setEnabled(true);
            	
            	array_list = mydb.getContactsToday("sim3no"); 
            	System.out.println("test"+array_list.size());
            	if(array_list.size()>0)
                button3.setEnabled(false);
            	else
              	button3.setEnabled(true);
            	
            	array_list = mydb.getContactsToday("sim4no"); 
            	System.out.println("test"+array_list.size());
            	if(array_list.size()>0)
                button4.setEnabled(false);
            	else
                button4.setEnabled(true);
            	
            	
            	array_list = mydb.getContactsToday("sim5no"); 
            	System.out.println("test"+array_list.size());
            	if(array_list.size()>0)
                button5.setEnabled(false);
            	else
               	button5.setEnabled(true);
            	
            	
            	array_list = mydb.getContactsToday("sim6no"); 
            	System.out.println("test"+array_list.size());
            	if(array_list.size()>0)
                button6.setEnabled(false);
            	else
                   	button6.setEnabled(true);
            	
            	array_list = mydb.getContactsToday("sim7no"); 
            	System.out.println("test"+array_list.size());
            	if(array_list.size()>0)
                button7.setEnabled(false);
            	else
                   	button7.setEnabled(true);
            	
            	array_list = mydb.getContactsToday("sim8no"); 
            	System.out.println("test"+array_list.size());
            	if(array_list.size()>0)
                button8.setEnabled(false);
            	else
                   	button8.setEnabled(true);
            	
            	array_list = mydb.getContactsToday("sim9no"); 
            	System.out.println("test"+array_list.size());
            	if(array_list.size()>0)
                button9.setEnabled(false);
            	else
                   	button9.setEnabled(true);
            	
            	array_list = mydb.getContactsToday("sim10no"); 
            	System.out.println("test"+array_list.size());
            	if(array_list.size()>0)
                button10.setEnabled(false);
            	else
                   	button10.setEnabled(true);
                // Perform action on click
            	
            }
        });
        button11.performClick();
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
   
    
    
    public void launchTestService() {
        // Construct our Intent specifying the Service
        Intent i = new Intent(this, MyTestService.class);
        // Add extras to the bundle
        i.putExtra("foo", "bar");
        // Start the service
        startService(i);
        
    }
    
    
}
